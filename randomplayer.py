"""
randomplayer.py
"""

import random
from player import Player

class RandomPlayer(Player):
    """
    Grandmaster of every game!
    Plays by picking a random (legal) move.
    """
    def turn(self, state):
        moves = state.possible_moves(self)
        if moves:
            return random.choice(moves) 
        else:
            return None
