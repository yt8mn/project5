"""
squareboard.py
"""

from board import Board

class Position:
    """
    (x, y) position on a board
    """

    def __init__(self, x, y):
        self.x = x
        self.y = y

    @staticmethod
    def parse(s):
        """
        Returns the Position corresponding to string s.
        s should be of the form '<int>, <int>'
        """
        comma = s.find(',')
        if comma == -1:
            raise ValueError("No comma in position.")
        xpos = int(s[:comma])
        ypos = int(s[comma + 1:])
        return Position(xpos, ypos)
    
    def __str__(self):
        return "(" + str(self.x) + ", " + str(self.y) + ")"
    
class SquareBoard(Board):
    """
    Square board (NxN positions arranged in a square).
    """

    def __init__(self, N):
        super().__init__(N * N)
        self._size = N

    def _convert_position(self, pos):
        if isinstance(pos, Position):
            return pos.x + (pos.y * self._size)
        else:
            return pos

    def _convert_to_position(self, pos):
        res = Position(pos % self._size, pos // self._size)
        assert self._convert_position(res) == pos
        return res
            
    def valid_position(self, pos):
        return super().valid_position(self._convert_position(pos))

    def empty_positions(self):
        return [self._convert_to_position(pos)
                for pos in super().empty_positions()]
    
    def has_object(self, pos):
        return super().has_object(self._convert_position(pos))

    def get_object(self, pos):
        return super().get_object(self._convert_position(pos))
        
    def remove_object(self, pos):
        return super().remove_object(self._convert_position(pos))

    def place_object(self, pos, obj):
        return super().place_object(self._convert_position(pos), obj)

    def __str__(self):            
        return "[" \
               + '\n '.join([' '.join([self.obj_unparse(self.get_object(Position(x, y)))
                                  for x in range(self._size)])    
                        for y in range(self._size)]) \
                    + "]"


def test_square():
    b = SquareBoard(3)
    print ('[%s]' % ', '.join([str(pos) for pos in b.empty_positions()]))
    assert b.get_object(Position(1,1)) == None
    b.place_object(Position(1, 1), "X")
    assert b.get_object(Position(1, 1)) == "X"
    b.place_object(Position(1, 2), "O")
    print ('[%s]' % ', '.join([str(pos) for pos in b.empty_positions()]))
    assert b.get_object(Position(1, 2)) == "O"
    print (str(b))
    b.remove_object(Position(1, 2))
    assert b.get_object(Position(1, 2)) == None
    print (str(b))      
